//
//  DefaultsKeys.swift
//  Country
//
//

import Foundation
import SwiftyUserDefaults

/**
Keys for NSUserDefaults used in the app.
*/
extension DefaultsKeys {
    static let adsPurchased = DefaultsKey<Bool>("adsPurchased")
}
