/* Get Pray Time Details */

var baseUrl = "https://binjassar.com/service";

var prayTime = new Vue({
  el: '#prayTime',
  data () {
    return {
      info: [],
      imgUrl: baseUrl
    }
  },
  mounted () {

      axios
      .get(baseUrl+'/service.php?action=pray_time')
      .then(response => (this.info = response.data))

  }
})

  