$(document).ready(function() {
	$('#btnSave').on('click', function() {
		
		var name = $('#name').val();
		var nid = $('#nid').val();
        var phone = $('#phone').val();
        var gender = $('#gender').val();
        var age = $('#age').val();
        var password = $('#password').val();

		if(name!="" && nid!="" && phone!="" && age!="" && password!="" ){
			$.ajax({
				url: "https://binjassar.com/service/send.php",
				type: "POST",
				data: {
					type: 1,
					name: name,
					nid: nid,
					phone: phone,
					gender: gender,
                    age: age,
                    password: password						
                },
                cache: false,
	            headers: { "cache-control": "no-cache" },
                beforeSend: function(){	
                    $("#error").fadeOut();
                    $("#success").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; جاري ارسال البيانات');
                },
				success : function(response){			
                    if($.trim(response) === "200"){
                        console.log('Done ....');									
                        $("#btnSave").html('تم تسجيل حسابك بنجاح');
                        setTimeout(' window.location.href = "login.html"; ',1000);
                    } else {									
                        $("#error").fadeIn(1000, function(){						
                            $("#error").html("رقم الهويه مسجل من قبل").show();
                        });
                    }
                }
			});
		}
		else{
			$("#error").show();
            $('#error').html('جميع البيانات مطلوبه للتسجيل');

            
        }
        return false;
	});
});