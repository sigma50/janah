/* Get chat Details */

var baseUrl = "https://binjassar.com/service";

var history = new Vue({
  el: '#chat',
  data () {
    return {
      info: [],
      imgUrl: baseUrl
    }
  },
  mounted () {

      axios
      .get(baseUrl+'/service.php?action=chat')
      .then(response => (this.info = response.data))

  }
});

$(document).ready(function(){

	$('#btnLogin').on('click', function() {
		var data = $("#login_form").serialize();
		$.ajax({				
			type : 'POST',
			url  : 'https://binjassar.com/service/service/send.php',
            data : data,
            cache: false,
	        headers: { "cache-control": "no-cache" },
			beforeSend: function(){	
				$("#error").fadeOut();
				// $("#error").html('<span class="glyphicon glyphicon-transfer"></span> &nbsp; جاري معالجة الطلب');
			},
			success : function(response){			
				if($.trim(response) === "200"){
					console.log('Done ....');									
					$("#btnLogin").html('جاري تسجيل الدخول');
					setTimeout(' window.location.href = "index.html"; ',200);
				} else {									
					$("#error").fadeIn(200, function(){						
						$("#error").html("رقم الهويه او كلمة المرور غير صحيح").show();
					});
				}
			}
		});
		return false;
	});

});	

  